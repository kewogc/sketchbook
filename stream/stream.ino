
#include "SPI.h"
#include "Adafruit_WS2801.h"

uint8_t dataPin  = 3;    // Yellow wire on Adafruit Pixels
uint8_t clockPin = 4;    // Green wire on Adafruit Pixels

template<class T> inline Print &operator <<(Print &obj, T arg) { obj.print(arg); return obj; }

Adafruit_WS2801 strip = Adafruit_WS2801((uint16_t)10, (uint16_t)10, dataPin, clockPin);

unsigned long counter = 0;

#define LED_DDR  DDRD
#define LED_PORT PORTD
#define LED_PIN  _BV(PORTD3)

const int numPixels = 100;
const int NCOLORS = 3;
static const uint8_t magic[] = {'A','d','a'};
#define MAGICSIZE  sizeof(magic)
#define HEADERSIZE (MAGICSIZE + 3)

#define MODE_HEADER 0
#define MODE_HOLD   1
#define MODE_DATA   2

static const unsigned long serialTimeout = 1500; // 15 seconds

uint8_t
    buffer[256],
    indexIn       = 0,
    indexOut      = 0,
    mode          = MODE_HEADER,
    hi, lo, chk, i, spiFlag;
  int16_t
    bytesBuffered = 0,
    hold          = 0,
    c;
  int32_t
    bytesRemaining;
  unsigned long
    startTime,
    lastByteTime,
    lastAckTime,
    t;
    
volatile int state = 2;
int frame_index;

uint8_t pixelbuf[numPixels*NCOLORS];
char data[4] = {0,0,0,0}; //4 character max e.g. "255,"
int16_t serial_c;

void setup () {
  
  startTime    = micros();
  lastByteTime = lastAckTime = millis();
  
  Serial.begin(115200);
  
  strip.begin();
  strip.show();
  frame_index=0;
  
  for(;;) {
    switch (state){ 
    case 0: {
      startup(4); 
    }
    break;
    case 1:{ 
      rainbow(50);
    }
    break;
    case 2:{
      LEDstream();
    }
    break;
    case 3:{
    }
    break; 
    }
  } 
}

void loop(){}

void LEDstream(){
    // Implementation is a simple finite-state machine.
    // Regardless of mode, check for serial input each time:
    t = millis();
    if((bytesBuffered < 256) && ((c = Serial.read()) >= 0)) {
      buffer[indexIn++] = c;
      bytesBuffered++;
      lastByteTime = lastAckTime = t; // Reset timeout counters
    } 
    else {
      // No data received.  If this persists, send an ACK packet
      // to host once every second to alert it to our presence.
      if((t - lastAckTime) > 1000) {
        Serial.print("Ada\n"); // Send ACK string to host
        lastAckTime = t; // Reset counter
      }
      // If no data received for an extended time, turn off all LEDs.
      if((t - lastByteTime) > serialTimeout) {
        for(c=0; c<32767; c++) {
          for(SPDR=0; !(SPSR & _BV(SPIF)); );
        }
        delay(1); // One millisecond pause = latch
        lastByteTime = t; // Reset counter
      }
    }
    switch(mode) {

     case MODE_HEADER:

      // In header-seeking mode.  Is there enough data to check?
      if(bytesBuffered >= HEADERSIZE) {
        // Indeed.  Check for a 'magic word' match.
        for(i=0; (i<MAGICSIZE) && (buffer[indexOut++] == magic[i++]););
        if(i == MAGICSIZE) {
          // Magic word matches.  Now how about the checksum?
          hi  = buffer[indexOut++];
          lo  = buffer[indexOut++];
          chk = buffer[indexOut++];
          if(chk == (hi ^ lo ^ 0x55)) {
            // Checksum looks valid.  Get 16-bit LED count, add 1
            // (# LEDs is always > 0) and multiply by 3 for R,G,B.
            bytesRemaining = 3L * (256L * (long)hi + (long)lo + 1L);
            bytesBuffered -= 3;
            spiFlag        = 0;         // No data out yet
            mode           = MODE_HOLD; // Proceed to latch wait mode
          } else {
            // Checksum didn't match; search resumes after magic word.
            indexOut  -= 3; // Rewind
          }
        } // else no header match.  Resume at first mismatched byte.
        bytesBuffered -= i;
      }
      break;

     case MODE_HOLD:

      // Ostensibly "waiting for the latch from the prior frame
      // to complete" mode, but may also revert to this mode when
      // underrun prevention necessitates a delay.

      if((micros() - startTime) < hold) break; // Still holding; keep buffering

      // Latch/delay complete.  Advance to data-issuing mode...

      mode      = MODE_DATA; // ...and fall through (no break):

     case MODE_DATA:

      while(spiFlag && !(SPSR & _BV(SPIF))); // Wait for prior byte
      if(bytesRemaining > 0) {
        if(bytesBuffered > 0) {
          SPDR = buffer[indexOut++];   // Issue next byte
          bytesBuffered--;
          bytesRemaining--;
          spiFlag = 1;
        }
        // If serial buffer is threatening to underrun, start
        // introducing progressively longer pauses to allow more
        // data to arrive (up to a point).
        if((bytesBuffered < 32) && (bytesRemaining > bytesBuffered)) {
          startTime = micros();
          hold      = 100 + (32 - bytesBuffered) * 10;
          mode      = MODE_HOLD;
	}
      } else {
        // End of data -- issue latch:
        startTime  = micros();
        hold       = 1000;        // Latch duration = 1000 uS
        mode       = MODE_HEADER; // Begin next header search
      }
    } // end switch
}

/*void serialEvent() {
  while (Serial.available()) {
    //char inChar = (char)Serial.read();
    
    int id = Serial.parseInt();
    if (id > 0){
      id = id-1;
      int red = Serial.parseInt();
      int green = Serial.parseInt();
      int blue = Serial.parseInt();
      strip.setPixelColor(id, Color(red, green, blue));
    
      counter+=1;
      if (counter > strip.numPixels()){
        counter = 0;
        strip.show();
      }
      //Serial << "ID: " << id << " COLOR: " << red << " " << green << " " << blue << "\n";
    }
  }
}*/

uint32_t Color(byte r, byte g, byte b)
{
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}

void startup(uint8_t wait){
  int i;
  uint8_t j;
  if (frame_index<256){
    j = (uint8_t)(255*pow((float)(frame_index/255.0),2.2));
  }
  else if (frame_index<512){
    j = (uint8_t)(255*pow((float)((255-frame_index%255)/255.0),2.2));
  }
  else{
    state++;
  }
  for (i=0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, j, j, j);
  }  
  strip.show();
  delay(wait);
  frame_index++;
}
void rainbow(uint8_t wait) {
  int i, j;
  j = frame_index%256;
  for (i=0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, Wheel( (i + j) % 255));
  }  
  strip.show();   // write all the pixels out
  delay(wait);
  frame_index++;
}
/* Helper functions */
uint32_t Wheel(byte WheelPos)
{
  if (WheelPos < 85) {
   return Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if (WheelPos < 170) {
   WheelPos -= 85;
   return Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
   WheelPos -= 170; 
   return Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}

