#include "FastLED.h"

#define DATA_PIN 2
#define CLOCK_PIN 3
#define NUM_LEDS 100

CRGB leds[NUM_LEDS];

void setup() {
  Serial.begin(115200);
  FastLED.addLeds<WS2801, DATA_PIN, CLOCK_PIN, RGB>(leds, NUM_LEDS);
  leds[0] = CRGB::Red;
  leds[1] = CRGB::Green;
  leds[2] = CRGB::Blue;
  FastLED.show();
}

void loop(){
  while (Serial.available() > 0) {
    Serial.readBytes( (char*)leds, NUM_LEDS * 3);
    FastLED.show();
  }
}

void establishContact() {
  
}

