require 'RMagick'
require "serialport"
include Magick

option = ARGV.shift
file = ''
is_loop = false
if option == '--image'
  file = ARGV.shift
end
option = ARGV.shift
if option == '--loop'
  is_loop = true
end


img = ImageList.new(file)
#fill = Magick::GradientFill.new(0, 0, 0, 0, "#000000", "#000000")
#dst = Magick::Image.new(img.first.page.width, img.first.page.height, fill)

sp = SerialPort.new("/dev/ttyUSB0",115200, 8, 1, SerialPort::NONE)
i = 0
loop do
  img.each do |m|
    sp.write m.resize_to_fit(10,10).export_pixels(0,0,10,10,"RGB").collect{|c| c >> 8}.pack('c*')
    sleep(10/m.ticks_per_second) if m.ticks_per_second > 0 
  end
  unless is_loop
    i-=1
    break if i < 1
  end
end
#sp.write [12,51,51].pack('c*') #d.shift(10).pack('c*')

#pixels = img.export_pixels(0,0,10,10,"RGB")
