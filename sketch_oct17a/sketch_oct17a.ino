#include "EtherShield.h"
#include "ETHER_28J60.h"
 
static uint8_t mac[6] = {0x54, 0x55, 0x58, 0x10, 0x00, 0x24};   
static uint8_t ip[4] = {10, 0, 0, 2};                      
static uint16_t port = 80;                                    
 
ETHER_28J60 ethernet;
 
void setup()
{ 
  ethernet.setup(mac, ip, port);
}
 
void loop()
{
  if (ethernet.serviceRequest())
  {
    ethernet.print("<H1>Hello World</H1>");
    ethernet.respond();
  }
  delay(100);
}
