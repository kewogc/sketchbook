
#include <SPI.h>
#include "Adafruit_WS2801.h"
#include "nRF24L01.h"
#include "RF24.h"
//#include <StackArray.h>

//StackArray <byte> stack;

uint8_t 
  indexln=0,
  indexOut=0;

typedef struct{
  int x,y;
  bool show;
  uint32_t color;
}
Color_t;
Color_t color;

uint8_t b;
uint8_t colorBuffer[3];
uint16_t buffer[256];
int8_t bytesBuffer=0;
bool show = false;

// RADIO
RF24 radio(9,10);
const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };


// LED
uint8_t dataPin  = 2;    // Yellow wire on Adafruit Pixels
uint8_t clockPin = 3;    // Green wire on Adafruit Pixels
//Adafruit_WS2801 strip = Adafruit_WS2801(100, dataPin, clockPin);
Adafruit_WS2801 strip = Adafruit_WS2801((uint16_t)10, (uint16_t)10, dataPin, clockPin);

//uint32_t colors[100];

//int mode_counter = 0;

void setup() {
  Serial.begin(115200);
  radio.begin();  
  strip.begin();
  
  radio.setDataRate(RF24_2MBPS);
  //radio.setPALevel(RF24_PA_MAX);
  //radio.setChannel(70);
  
  radio.enableDynamicPayloads();
  //radio.setPayloadSize(8);
  // radio.setRetries(15,15);
  //radio.setCRCLength(RF24_CRC_16);

  
  

  // Update LED contents, to start they are all 'off'
  strip.show();
  
  strip.setPixelColor(0, Color(255, 0, 0));
  strip.setPixelColor(1, Color(0, 255, 0));
  strip.setPixelColor(2, Color(0, 0, 255));
  strip.show();
  
  radio.openWritingPipe(pipes[1]);
  radio.openReadingPipe(1,pipes[0]);
  
  radio.startListening();
  //radio.printDetails();
}





void loop() {
  
  if ( radio.available() && bytesBuffer<256 ){
    bool done = false;
    while (!done){
      done = radio.read( &b, sizeof(b) );
    }
    if(done==true){
      //
      buffer[bytesBuffer] = b;
      bytesBuffer++;
      Serial.println(b);
    }
  }else{
    
    if (bytesBuffer>2){
      indexOut=0;
      while (bytesBuffer>=0){
        colorBuffer[indexln] = buffer[indexOut];
        indexOut++;
        indexln++;
        if(indexln>2){
          Serial.print(colorBuffer[0]);
          Serial.print(colorBuffer[1]);
          Serial.print(colorBuffer[2]);
          indexln=0;
          strip.setPixelColor(3, Color(colorBuffer[0], colorBuffer[1], colorBuffer[2]));
          //strip.setPixelColor(0, Color(stack.pop(), stack.pop(), stack.pop()));
          show = true;
        }
        bytesBuffer--;
      }
      //if(bytesBuffer<0){ bytesBuffer = 0;}
      if(show){
        Serial.println("SHOW");
        strip.show();
        show = false;
        bytesBuffer = 0;
      }
    }
    //Serial.println("---");
  }
}



/* Helper functions */

// Create a 24 bit color value from R,G,B
uint32_t Color(byte r, byte g, byte b)
{
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}
