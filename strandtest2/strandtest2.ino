#include <SPI.h>
#include "Adafruit_WS2801.h"
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"

typedef struct{
  int x,y;
  bool show;
  uint32_t color;
}
Color_t;
Color_t color;

// RADIO
RF24 radio(9,10);
const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };

// LED
uint8_t dataPin  = 2;    // Yellow wire on Adafruit Pixels
uint8_t clockPin = 3;    // Green wire on Adafruit Pixels
//Adafruit_WS2801 strip = Adafruit_WS2801(100, dataPin, clockPin);
Adafruit_WS2801 strip = Adafruit_WS2801((uint16_t)10, (uint16_t)10, dataPin, clockPin);

//uint32_t colors[100];

//int mode_counter = 0;

void setup() {
  radio.begin();  
  strip.begin();
  
  radio.setDataRate(RF24_2MBPS);
  //radio.setPALevel(RF24_PA_MAX);
  //radio.setChannel(70);
  
  radio.enableDynamicPayloads();
  //radio.setPayloadSize(8);
  // radio.setRetries(15,15);
  //radio.setCRCLength(RF24_CRC_16);

  
  Serial.begin(57600);
  printf_begin();

  // Update LED contents, to start they are all 'off'
  strip.show();
  
  strip.setPixelColor(0, Color(255, 0, 0));
  strip.setPixelColor(1, Color(0, 255, 0));
  strip.setPixelColor(2, Color(0, 0, 255));
  strip.show();
  
  radio.openWritingPipe(pipes[1]);
  radio.openReadingPipe(1,pipes[0]);
  
  radio.startListening();
  //radio.printDetails();
}


void loop() {
  
    // if there is data ready
    if ( radio.available() )
    {
      bool done = false;
      while (!done){
        done = radio.read( &color, sizeof(color) );
      }
      strip.setPixelColor(color.x, color.y, color.color);
      if(color.show == true){
        strip.show();
      }
      printf("Got %i:%i %lu \n", color.x, color.y, color.color);
      
      
      //radio.stopListening();
      //radio.write( &color, sizeof(uint32_t) );
      //radio.startListening();
    }
  

}



/* Helper functions */

// Create a 24 bit color value from R,G,B
uint32_t Color(byte r, byte g, byte b)
{
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}
