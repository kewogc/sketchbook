#include <SPI.h>
#include "Adafruit_WS2801.h"
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"

// RADIO
RF24 radio(9,10);
const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };

// LED
uint8_t dataPin  = 2;    // Yellow wire on Adafruit Pixels
uint8_t clockPin = 3;    // Green wire on Adafruit Pixels
Adafruit_WS2801 strip = Adafruit_WS2801(100, dataPin, clockPin);

//uint32_t colors[100];

int mode_counter = 0;

void setup() {
  radio.begin();  
  strip.begin();
  
  radio.setDataRate(RF24_1MBPS);
  //radio.setPALevel(RF24_PA_MAX);
  //radio.setChannel(70);
  
  radio.enableDynamicPayloads();
  // radio.setRetries(15,15);
  //radio.setCRCLength(RF24_CRC_16);
  
  Serial.begin(57600);
  printf_begin();

  // Update LED contents, to start they are all 'off'
  strip.show();
  
  strip.setPixelColor(0, Color(255, 0, 0));
  strip.setPixelColor(1, Color(0, 255, 0));
  strip.setPixelColor(2, Color(0, 0, 255));
  strip.show();
  
  radio.openWritingPipe(pipes[1]);
  radio.openReadingPipe(1,pipes[0]);
  
  radio.startListening();
  radio.printDetails();
}


void loop() {
  
    // if there is data ready
    if ( radio.available() )
    {
      uint32_t color;
      bool done = false;
      bool show = false;
      while (!done){
        done = radio.read( &color, sizeof(uint32_t) );
	//delay(20);
      }
      printf("Got color %lu \n\r", color);
      strip.setPixelColor(mode_counter, color);
      mode_counter++;
      if(mode_counter>=100){
        mode_counter = 0;
        strip.show();
      }
      //printf("Got color \n\r");
      //for(int i=0; i<sizeof(colors); i++){
      //  strip.setPixelColor(i, colors[i]);
      //}
      //strip.show();
      
      //radio.stopListening();
      //radio.write( &color, sizeof(uint32_t) );
      //radio.startListening();
    }
  

}



/* Helper functions */

// Create a 24 bit color value from R,G,B
uint32_t Color(byte r, byte g, byte b)
{
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}
