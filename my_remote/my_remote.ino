// A simple web server that always just says "Hello World"
#include "SPI.h"
#include "Adafruit_WS2801.h"
#include "EtherShield.h"
#include "ETHER_28J60.h"
#include <aJSON.h>
#include <Ethernet.h>

uint8_t dataPin  = 3;    // Yellow wire on Adafruit Pixels
uint8_t clockPin = 4;    // Green wire on Adafruit Pixels

enum eMode {
  mode_off,
  mode_play,
  mode_load,
  mode_post
};

eMode mode = mode_off;

int outputPin = 6;

static uint8_t mac[6] = {0x54, 0x55, 0x58, 0x10, 0x00, 0x24};   // this just needs to be unique for your network, 
                                                                // so unless you have more than one of these boards
                                                                // connected, you should be fine with this value.
                                                           
static uint8_t ip[4] = {10, 0, 0, 2};                       // the IP address for your board. Check your home hub
                                                                // to find an IP address not in use and pick that
                                                                // this or 10.0.0.15 are likely formats for an address
                                                                // that will work.

static uint16_t port = 80;                                      // Use port 80 - the standard for HTTP

ETHER_28J60 e;

Adafruit_WS2801 strip = Adafruit_WS2801((uint16_t)10, (uint16_t)10, dataPin, clockPin);

//String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete

EthernetClient client;
char server[] = "10.0.0.2";

unsigned long lastConnectionTime = 0;          // last time you connected to the server, in milliseconds
boolean lastConnected = false;                 // state of the connection last time through the main loop
const unsigned long postingInterval = 3*1000;  // delay between updates, in milliseconds

void setup()
{ 
  
  Serial.begin(9600);
  // this check is only needed on the Leonardo:
   while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  
  e.setup(mac, ip, port);
  //pinMode(outputPin, OUTPUT);
  strip.begin();
  strip.show();
  
}

void httpRequest() {
  // if there's a successful connection:
  if (client.connect(server, 3000)) {
    Serial.println("connecting...");
    // send the HTTP PUT request:
    client.println("GET /api/pictures HTTP/1.1");
    client.println("Host: 10.0.0.2:3000");
    client.println("User-Agent: arduino-ethernet");
    client.println("Connection: close");
    client.println();

    // note the time that the connection was made:
    lastConnectionTime = millis();
  } 
  else {
    // if you couldn't make a connection:
    Serial.println("connection failed");
    Serial.println("disconnecting.");
    client.stop();
  }
}


void loop()
{
  if( mode == mode_off || mode == mode_play || mode == mode_load){
    char* params;
    if (params = e.serviceRequest())
    {
      Serial.println(params);  
      e.print("<H1>Web Remote</H1>");
      if (mode == mode_off)
      {
        e.print("Current mode is Off");
      }
      if (mode == mode_play)
      {
        e.print("Current mode is Play");
      }
      if (mode == mode_load)
      {
        e.print("Current mode is Load");
      }
      
      if (mode == mode_load){
        int i = atoi(params);
        if (i>0){
          if (i > strip.numPixels() ) i = 1;
          strip.setPixelColor(i-1, Color(255, 255, 255));
          strip.show();
        }
      }
      e.respond();
    }
  }

  if (mode == mode_post){
    if (!client.connected() && lastConnected) {
      Serial.println();
      Serial.println("disconnecting.");
      client.stop();
    }
  
    // if you're not connected, and ten seconds have passed since
    // your last connection, then connect again and send data:
    if(!client.connected() && (millis() - lastConnectionTime > postingInterval)) {
      httpRequest();
    }
    // store the state of the connection for next time through
    // the loop:
    lastConnected = client.connected();
  }
  
}

void serialEvent() {
  while (Serial.available()) {
    //char inChar = (char)Serial.read();
    int m = Serial.parseInt();
   
    if (m > 0) {
      if (m == 1){
        mode = mode_off;
        Serial.println("mode - off");
      }
      if (m == 2){
        mode = mode_play;
        Serial.println("mode - play");
      }
      if (m == 3){
        mode = mode_load;
        Serial.println("mode - load");
      }
      if (m == 4){
        mode = mode_post;
        Serial.println("mode - post");
      }
    } 
  }
}
void clearColor() {
  for (int i=0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, Color(0, 0, 0));
  } 
  strip.show();
}

// Create a 24 bit color value from R,G,B
uint32_t Color(byte r, byte g, byte b)
{
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}

//Input a value 0 to 255 to get a color value.
//The colours are a transition r - g -b - back to r
uint32_t Wheel(byte WheelPos)
{
  if (WheelPos < 85) {
   return Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if (WheelPos < 170) {
   WheelPos -= 85;
   return Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
   WheelPos -= 170; 
   return Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}
