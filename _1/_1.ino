
#include "SPI.h"
#include "Adafruit_WS2801.h"

uint8_t dataPin  = 3;    // Yellow wire on Adafruit Pixels
uint8_t clockPin = 4;    // Green wire on Adafruit Pixels

template<class T> inline Print &operator <<(Print &obj, T arg) { obj.print(arg); return obj; }

Adafruit_WS2801 strip = Adafruit_WS2801((uint16_t)10, (uint16_t)10, dataPin, clockPin);

unsigned long counter = 0;
boolean load = true;
boolean play = false;


void setup () {
  Serial.begin(115200);
  
  strip.begin();
  strip.show();
  Serial << "Давай мне данные в формате: ID,RED,GREEN,BLUE\n";
  Serial << "ID - от 1 до " << strip.numPixels() << "\n";
  Serial << "RED,GREEN,BLUE - 0 .. 255\n";
}

void loop(){
  if(load == false){
    load = true;
    strip.show();
  }
}

void serialEvent() {
  while (Serial.available()) {
    //char inChar = (char)Serial.read();
    
    int id = Serial.parseInt();
    if (id > 0){
      id = id-1;
      int red = Serial.parseInt();
      int green = Serial.parseInt();
      int blue = Serial.parseInt();
      strip.setPixelColor(id, Color(red, green, blue));
    
      counter+=1;
      if (counter > strip.numPixels()){
        counter = 0;
        load = false;
      }
      Serial << "ID: " << id << " COLOR: " << red << " " << green << " " << blue << "\n";
    }
  }
}

uint32_t Color(byte r, byte g, byte b)
{
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}

