
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"


RF24 radio(9,10);

// Radio pipe addresses for the 2 nodes to communicate.
const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };

//uint32_t colors[100];

void setup(void)
{
  //
  // Print preamble
  //

  //Serial.begin(57600);
  //printf_begin();


  radio.begin();
  radio.setDataRate(RF24_1MBPS);
  //radio.setDataRate(RF24_250KBPS);
  //radio.setPALevel(RF24_PA_MAX);
  //radio.setChannel(70);
  
  radio.enableDynamicPayloads();
  // radio.setRetries(15,15);
  //radio.setCRCLength(RF24_CRC_16);

  //radio.setRetries(15,15);

  // optionally, reduce the payload size.  seems to
  // improve reliability
  //radio.setPayloadSize(8);

  //
  // Open pipes to other nodes for communication
  //

  // This simple sketch opens two pipes for these two nodes to communicate
  // back and forth.
  // Open 'our' pipe for writing
  // Open the 'other' pipe for reading, in position #1 (we can have up to 5 pipes open for reading)

  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1,pipes[1]);
  //radio.printDetails();
  radio.startListening();
}

void loop(void)
{
  
  while (Serial.available() > 0) {
    int red = Serial.parseInt(); 
    int green = Serial.parseInt(); 
    int blue = Serial.parseInt(); 

    if (Serial.read() == '\n') {
      red = constrain(red, 0, 255);
      green = constrain(green, 0, 255);
      blue = constrain(blue, 0, 255);
      
      
      //printf("Color %i, %i, %i\n\r",red,green,blue);
      
      
      // First, stop listening so we can talk.
      radio.stopListening();
  
      for(int i=0; i<100; i++){
       uint32_t color = Color(red,green,blue);
       bool ok = radio.write( &color, sizeof(uint32_t) );
       //if (ok)
       //  printf("ok...");
       //else
       //  printf("failed.\n\r");
      }
      //bool ok = radio.write( &colors, sizeof(uint32_t)*100 );
  
      radio.startListening();
      
    }
  } 
  
}

/* Helper functions */

// Create a 24 bit color value from R,G,B
uint32_t Color(byte r, byte g, byte b)
{
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}
