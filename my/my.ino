#include "EtherCard.h"
//#include "SoftwareSerial.h"
//#include "avr/pgmspace.h"
#include "aJSON.h"
template<class T> inline Print &operator <<(Print &obj, T arg) { obj.print(arg); return obj; }
const int
  printer_RX_Pin = 5,  // Printer connection: green wire
  printer_TX_Pin = 4;  // Printer connection: yellow wire
  
// ethernet interface mac address, must be unique on the LAN
static byte mac[] = { 0x74,0x69,0x69,0x2D,0x30,0x31 };

byte Ethernet::buffer[1024];

char
website[] PROGMEM = "192.168.1.110";

byte
  state = 0;

// called when the client request is complete
static void browseUrlCallback1 (byte status, word off, word len) {
   Serial.print("<<<<<<<<<<<<>>>>>>>>>>>>>>");
   char *pos;// used for buffer searching
   Ethernet::buffer[off+len] = 0;
   pos=(char *)(Ethernet::buffer+off);
  // set the byte after the end of the buffer to zero to act as an end marker (also handy for displaying the buffer as a string)
  Serial.println(pos);
}

void connectWWW() {
  Serial.println("Initializing Ethernet...");
  for( int i=0; i<6; i++ ) {
    Serial.print( mac[i], HEX );
    Serial.print( i < 5 ? ":" : "" );
  }
  Serial.println();

  if (ether.begin(sizeof Ethernet::buffer, mac) == 0){ 
    Serial.println( "Failed to access Ethernet controller");
    return;
  }
  
  Serial.println("Requesting IP Address...");
  if (!ether.dhcpSetup()){
    Serial.println("DHCP failed");
    return;
  }
  // Get IP Address details
  ether.printIp("IP:  ", ether.myip);
  ether.printIp("GW:  ", ether.gwip);  
  ether.printIp("DNS: ", ether.dnsip); 

  
  Serial.println("Requesting HOST DNS...");
  if (!ether.dnsLookup(website)){
    Serial.println("HOST DNS failed...");
    return;
  }
    
  ether.printIp("HOST DNS success: ", ether.hisip);
  Serial.println(ether.hisport);
}

void setup () {
  Serial.begin(9600);
  Serial << "HELLO";
  connectWWW();
}

void loop(){
  
  word len = ether.packetReceive();
  word pos = ether.packetLoop(len);
  
  if (state != 1) {
    state = 1;
    ether.browseUrl(PSTR("/api/pictures"), "", website, browseUrlCallback1);
  }
}
