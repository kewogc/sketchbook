
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"

#define MODE_GET 0
#define MODE_HOLD   1
#define MODE_SEND   2

typedef struct{
  int x,y;
  bool show;
  uint32_t color;
}
Color_t;

Color_t color;

uint8_t 
  buffer[256],
  indexln = 0,
  indexOut = 0,
  indexColors = 0,
  mode = MODE_GET;
int16_t
  bytesBuffered = 0,
  colorsBuffered = 0,
  c;
int32_t 
  bytesRemaining;

RF24 radio(9,10);

uint32_t bufColors[256];


bool show = false;

// Radio pipe addresses for the 2 nodes to communicate.
const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };

char cByte[3];
//StackArray <uint32_t> colors;
bool send_now = false;
uint8_t indexByte = 0;
uint8_t indexColor = 0;

void setup(void)
{
  Serial.begin(115200);
  printf_begin();
  radio.begin();
  radio.setDataRate(RF24_2MBPS);
  //radio.setDataRate(RF24_250KBPS);
  //radio.setPALevel(RF24_PA_MAX);
  //radio.setChannel(70);
  radio.enableDynamicPayloads();
  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1,pipes[1]);
  radio.printDetails();
  radio.startListening();
}

void loop(void)
{
  indexln = 0;
  while (Serial.available() > 0) {
    if( (c = Serial.read())>=0 ){
      buffer[indexln++] = c;
      printf("got %i %i\n", indexln-1, buffer[indexln-1]);
      if(indexln>2){
        indexln = 0;
        bufColors[indexColors++] = Color(buffer[0],buffer[1],buffer[2]);
        colorsBuffered++;  
        //color.x = 0;
        //color.y = 0;
        //color.show = 1;
        //color.color = Color(buffer[0],buffer[1],buffer[2]); 
        //bool ok = radio.write( &color, sizeof(color));
        printf("color %i %i %i = %lu\n",buffer[0],buffer[1],buffer[2],color.color);
        indexOut = 0;
      }
    }
  } 
  if(colorsBuffered>0 && indexOut>=0){
    color.x = 0;
    color.y = 0;
    color.show = 1;
    color.color = bufColors[indexOut--]; 
    bool ok = radio.write( &color, sizeof(color));
    printf("color %lu\n", color.color);
    colorsBuffered--;
  }
  //if (mode == MODE_SEND) mode = MODE_HOLD;
  
  

}


/* Helper functions */

// Create a 24 bit color value from R,G,B
uint32_t Color(byte r, byte g, byte b)
{
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}

