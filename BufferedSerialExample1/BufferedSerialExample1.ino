
#include <BufferedSerial.h>
#include <ByteBuffer.h>

BufferedSerial serial = BufferedSerial(256, 256);
ByteBuffer send_buffer;

int SEND_EXAMPLE = 3;

void setup()
{
  // initialize the serial communication:
  serial.init(0, 115200);
  serial.setPacketHandler(handlePacket);
 
  // Initialize the send buffer that we will use to send data
  send_buffer.init(30);
  send_buffer.put("123");
}

void loop() {
  serial.update();
 
  // If we are not busy sending then lets send something
  if( !serial.isBusySending() ){
    // Send some dummy data  
    send_buffer.clear();
    send_buffer.put(17);
    send_buffer.putInt(300);    
    send_buffer.putLong(-100000);    
    send_buffer.putFloat(3.14);    
  }
}

void handlePacket(ByteBuffer* packet){


  send_buffer.clear();
  while( packet->getSize() > 0 )
    send_buffer.put( packet->get() );
   serial.sendSerialPacket( &send_buffer );
 
}

