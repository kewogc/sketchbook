
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"

typedef struct{
  int x,y;
  bool show;
  uint32_t color;
}
Color_t;

Color_t color;


RF24 radio(9,10);

// Radio pipe addresses for the 2 nodes to communicate.
const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };

uint32_t colors[100];
//StackArray <uint32_t> colors;

void setup(void)
{
  //
  // Print preamble
  //

  Serial.begin(57600);
  printf_begin();
  
  //for(int i=0; i<100; i++){
  //  colors[i] = Color(i+50,i+100,i);
  //}
  
  //printf("Size of colors %i \n\r", sizeof(colors));
  
  //for(int i=0; i<count; i=i+8){ 
  //  uint32_t *buf; 
  //  for(int b=0; b<8; b++){
  //    if(i+b>=count) break;
  //   memcpy(buf,&colors[i+b],sizeof(uint32_t));
  //  }
  //  bool ok = radio.write( &buf, sizeof(buf));
  //  printf("Color %i\n\r", sizeof(buf));
  //}
  
  
  
 
  radio.begin();
  radio.setDataRate(RF24_2MBPS);
  //radio.setDataRate(RF24_250KBPS);
  //radio.setPALevel(RF24_PA_MAX);
  //radio.setChannel(70);
  
  radio.enableDynamicPayloads();
  // radio.setRetries(15,15);

  //radio.setRetries(15,15);

  // optionally, reduce the payload size.  seems to
  // improve reliability
  //radio.setPayloadSize(8);

  //
  // Open pipes to other nodes for communication
  //

  // This simple sketch opens two pipes for these two nodes to communicate
  // back and forth.
  // Open 'our' pipe for writing
  // Open the 'other' pipe for reading, in position #1 (we can have up to 5 pipes open for reading)

  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1,pipes[1]);
  radio.printDetails();
  radio.startListening();
}

void loop(void)
{
  
  while (Serial.available() > 0) {
    int x = Serial.parseInt(); 
    int y = Serial.parseInt();
    int red = Serial.parseInt(); 
    int green = Serial.parseInt(); 
    int blue = Serial.parseInt(); 
    int show_i = Serial.parseInt(); 
    bool show = false;
    if (Serial.read() == '\n') {
      x = constrain(x, 0, 9);
      y = constrain(y, 0, 9);
      red = constrain(red, 0, 255);
      green = constrain(green, 0, 255);
      blue = constrain(blue, 0, 255);
      show_i = constrain(show_i, 0, 1);
      if(show_i == 1) show = true;
      
      
      //printf("Color %i, %i, %i\n\r",red,green,blue);
      
      
      // First, stop listening so we can talk.
      radio.stopListening();
  
      //for(int i=0; i<100; i++){
       //colors[i] = Color(red,green,blue);
      color.x = x;
      color.y = y;
      color.show = show;
      color.color = Color(red,green,blue);
      
      bool ok = radio.write( &color, sizeof(color));
      
      printf("Color x:%i y:%i red:%i green:%i blue:%i show:%i \n\r",color.x, color.y, red, green, blue, color.show);
      //}
      
      radio.startListening();
      
    }
  } 
  
}


/* Helper functions */

// Create a 24 bit color value from R,G,B
uint32_t Color(byte r, byte g, byte b)
{
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}
