
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"


typedef struct{
  int x,y;
  bool show;
  uint32_t color;
}
Color_t;

Color_t color;

uint8_t 
  buffer[256],
  indexln = 0,
  indexOut = 0;
int16_t
  bytesBuffered = 0,
  c;
int32_t 
  bytesRemaining;

RF24 radio(9,10);

uint32_t colors[100];


bool show = false;

// Radio pipe addresses for the 2 nodes to communicate.
const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };

char cByte[3];
//StackArray <uint32_t> colors;
bool send_now = false;
uint8 indexByte = 0;
uint8 indexColor = 0;

void setup(void)
{
  Serial.begin(115200);
  printf_begin();
  radio.begin();
  radio.setDataRate(RF24_2MBPS);
  //radio.setDataRate(RF24_250KBPS);
  //radio.setPALevel(RF24_PA_MAX);
  //radio.setChannel(70);
  radio.enableDynamicPayloads();
  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1,pipes[1]);
  radio.printDetails();
  radio.startListening();
}

void loop(void)
{
  
  while (Serial.available() > 0) {
    cByte[indexByte++] = Serial.read();
    if(indexByte>=2){
      indexByte = 0;
      if(indexColor<100){
        colors[indexColor++] = Color(cByte[0],cByte[1],cByte[2]);
      }
    }
  } 
  if(indexColor>0){
    indexColor = 0;
    int r=0,c=0;
    for(i=0;i<100;i++){
      color.x = r;
      color.y = c;
      color.show = false;
      if (i==99) color.show = true; 
      color.color = Color(red,green,blue);
      bool ok = radio.write( &color, sizeof(color));
      r++;
      if(r>9){
        r = 0;
        c++;
        if(c>9){
         c = 0;
        }
      }
    }
  }

}


/* Helper functions */

// Create a 24 bit color value from R,G,B
uint32_t Color(byte r, byte g, byte b)
{
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}
