#include "SPI.h"
#include "Adafruit_WS2801.h"

uint8_t dataPin  = 2; 
uint8_t clockPin = 3; 
Adafruit_WS2801 strip = Adafruit_WS2801((uint16_t)10, (uint16_t)10, dataPin, clockPin);


uint8_t
  buffer[256],
  indexOut = 0;

int16_t
  bytesBuffered = 0;

int32_t
  bytesRemaining;
 

void setup() {
    
  strip.begin();
  /*strip.show();
  strip.setPixelColor(0, 9, 255, 0, 0);
  strip.setPixelColor(9, 0, 0, 255, 0);
  strip.setPixelColor(9, 9, 0, 0, 255);
  strip.setPixelColor(0, 0, 125, 125, 125);
  strip.show();
  */
  
  for(indexOut=0; indexOut<256; indexOut++){
    SPDR = indexOut;
  }
  strip.show();

}


void loop() {
  //while(!(SPSR & _BV(SPIF)));
}


uint32_t Color(byte r, byte g, byte b)
{
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}

