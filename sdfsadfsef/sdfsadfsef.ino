#include "FastLED.h"

#define DATA_PIN 2
#define CLOCK_PIN 3
#define NUM_LEDS 100

CRGB leds[NUM_LEDS];

char Buffer[512];  // Serial buffer
int ByteCount;

void setup() {
  Serial.begin(115200);
  FastLED.addLeds<WS2801, DATA_PIN, CLOCK_PIN, RGB>(leds, NUM_LEDS);
  leds[0] = CRGB::Red;
  FastLED.show();
}

void loop(){

  ByteCount = -1;
  ByteCount =  Serial.readBytesUntil('\n',Buffer,sizeof(Buffer));  
 
   if (ByteCount  > 0) {
     LEDS.showColor(CRGB(0, 0, 0));
     int led = 0;
     for(int i= 2; i<ByteCount; i=i+3){
       Serial.println(i);
       leds[led].setRGB( Buffer[i-2], Buffer[i-1], Buffer[i]);
       led++;
     }
     FastLED.show();
   }
   memset(Buffer, 0, sizeof(Buffer));   // Clear contents of Buffer
   Serial.flush();

}
